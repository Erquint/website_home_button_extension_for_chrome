# README #

### Overview ###

* Chrome pageAction extension that adds a context-sensitive button for returning to 2nd-level domain without a path of the current website.
* 0.2

### Installation ###

* Extension is soon to be released to Chrome Web Store.
* Meanwhile you can test it in developer mode.

### Upcoming/wanted features ###

* Set pageAction icon to tab's favicon if any on update.
* Support "exotic" domain names in accordance with https://publicsuffix.org/list/

### Contact ###

* You can try gness.na@gmail.com or google "Erquint". I'm all over the Internet.