'use strict';

function trimURL(tab) {
  var urlParts = tab.url.split("/");
  var domain = urlParts[2];
  var pos = 0;
  if (!(domain.match(/^www\./) && window.localStorage.getItem(domain))) {
    while ((pos = domain.indexOf(".")) != -1) {
      domain = domain.substring(pos + 1);
      if (window.localStorage.getItem(domain)) {
        break;
      }
    }
  }
  pos = urlParts[2].lastIndexOf(domain);
  chrome.tabs.update(tab.id, {
    url: urlParts[0] + "//" + urlParts[2].substring(urlParts[2].lastIndexOf(".",pos-2)+1) + "/"
  }, undefined);
}

function updatePageAction(tabId, changeInfo, tab) {
  if (changeInfo.status == "complete") {
    var urlParts = tab.url.split("/");
    var domain = urlParts[2];
    var pos = 0;
    while ((pos = domain.indexOf(".")) != -1) {
      domain = domain.substring(pos + 1);
      if (window.localStorage.getItem(domain)) {
        break;
      }
    }
    if (urlParts[3] || !(urlParts[2].match(/^www\./) || window.localStorage.getItem(urlParts[2]) || (urlParts[2].substring(0,urlParts[2].lastIndexOf(domain)-1).split(".").length < 2))) {
      chrome.pageAction.onClicked.removeListener(trimURL);
      chrome.pageAction.onClicked.addListener(trimURL);
      chrome.pageAction.show(tabId);
    } else {
      chrome.pageAction.hide(tabId);
    }
  }
}

function updateDomainList() {
  if (window.localStorage.getItem("updateDate") == null) {
    window.localStorage.setItem("updateDate", 0);
  }
  var timeDiff = Math.abs(new Date().getTime() - window.localStorage.getItem("updateDate"));
  var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
  if (diffDays >= 1) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "https://publicsuffix.org/list/public_suffix_list.dat", true);
    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        window.localStorage.clear();
        window.localStorage.setItem("updateDate", new Date().getTime());
        var domainList = xmlhttp.responseText.split("\n");
        for (var i = 0; i < domainList.length; i += 1) {
          if (!(domainList[i].trim() == "" || domainList[i].match(/^\/\//))) {
            if (domainList[i].match(/^\*\./)) {
              domainList[i] = domainList[i].substring(2, domainList[i].length);
            }
            if (domainList[i].match(/^\!/)) {
              domainList[i] = domainList[i].substring(1, domainList[i].length);
            }
            window.localStorage.setItem(domainList[i], true);
          }
        }
      }
    }
    xmlhttp.send();
  }
}

chrome.runtime.onStartup.addListener(updateDomainList);
chrome.tabs.onUpdated.addListener(updatePageAction);